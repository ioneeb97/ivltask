import { Component, OnInit } from '@angular/core';
import { loginData } from './../app-config';

// ...
import { FormBuilder, FormGroup, FormArray } from '@angular/forms';

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.css']
})
export class LoginComponent implements OnInit {

  loginData: any;
  characters: Array<string> = [];
  
  randomNumbers:Array<number> = [];

  constructor(private formBuilder: FormBuilder) { 
  }

  ngOnInit(): void {
    this.loginData = loginData;
    this.characters = this.loginData.password.split('');
    this.setRandomArray();
    this.setCharacterArray();
  }

  setRandomArray(){
    for(let m=0; this.randomNumbers.length <=3; m++){
      let num = this.getRandomNumber();
      let result = false;
      for(let i = 0;i<this.randomNumbers.length;i++){
        if(this.randomNumbers[i] == num){
          result = true;
          break;
        }
      }
      if(!result)this.randomNumbers.push(num);
    }
  }

  getRandomNumber(){
    return Math.floor((Math.random() * 7) + 1);
  }

  setCharacterArray(){
    this.randomNumbers.forEach((x) => {
      this.characters[x] = '';
    })
  }

  login(){
    const password = this.characters.join('');
    if(loginData.password == password){
      alert("Correct Pin");
    }else{
      alert("Wrong Pin");
      this.ngOnInit();
    }
  }
  enterWord(i: number,event: any){
    if((event.keyCode <= 90 && event.keyCode >= 65) || (event.keyCode >= 48 && event.keyCode <= 57)){
      this.characters[i] = event.key
    }
  }
}
